import jwt from "jsonwebtoken";

export const createdToken = (user: any) => {
  const { email, password } = user;
  return jwt.sign({ user }, "RavenJosh", { expiresIn: "1h" });
};
