import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity,
  ManyToMany,
  JoinTable,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { User } from "./User";
import { Category } from "./Category";

@Entity()
export class Product extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: true,
  })
  name: string;

  @Column()
  price: number;

  @Column()
  description: string;

  @Column()
  image: string;

  @ManyToMany(() => User, (user) => user.products)
  @JoinTable({
    name: "user_product",
    joinColumn: {
      name: "productId",
    },
    inverseJoinColumn: {
      name: "userId",
    },
  })
  users: User[];

  @ManyToOne(() => Category, (category) => category.products)
  @JoinColumn({
    name: "categoryId",
  })
  category: Category;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
