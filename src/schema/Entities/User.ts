import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity,
  ManyToMany,
} from "typeorm";
import { Product } from "./Product";

export type UserRole = "MANAGER" | "CLIENT";

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({
    unique: true,
    nullable: true,
  })
  email: string;

  @Column({
    nullable: true,
    select: false,
  })
  password: string;

  @Column({
    type: "enum",
    enum: ["MANAGER", "CLIENT"],
    default: "CLIENT",
  })
  role: string;

  @ManyToMany(() => Product, (product) => product.users)
  products: Product[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
