import {
  GraphQLFloat,
  GraphQLInt,
  GraphQLString,
  GraphQLObjectType,
  GraphQLSchema,
} from "graphql";
import { Product } from "../Entities/Product";
import { createdToken } from "../../Util/auth";

export const CREATE_PRODUCT = {
  description: "Create product ",
  type: GraphQLString,
  args: {
    name: { type: GraphQLString },
    description: { type: GraphQLString },
    price: { type: GraphQLFloat },
    category: { type: GraphQLInt },
    image: { type: GraphQLString },
  },
  async resolve(parent: any, args: any) {
    const { name, description, price, category, image } = args;
  },
};

// export const CREATE = {
//   description: "Sign up a new user and return a token",
//   type: GraphQLString,
//   args: {
//     name: { type: GraphQLString },
//     email: { type: GraphQLString },
//     password: { type: GraphQLString },
//     role: { type: GraphQLString },
//   },
//   async resolve(parent: any, args: any) {
//     const { name, email, password, role } = args;

//     await .insert({
//       name,
//       email,
//       password,
//       role,
//     }).catch((error) => {
//       throw new Error(error);
//     });
//     const user = { name, email, role };
//     const token = createdToken(user);

//     return token;
//   },
// };

// export const DELETE = {
//   type: GraphQLString,
//   args: {
//     name: { type: GraphQLString },
//     email: { type: GraphQLString },
//     password: { type: GraphQLString },
//   },

//   async resolve(parent: any, args: any) {
//     return "User logged out";
//   },
// };

// export const UPDATE = {
//   type: GraphQLString,
//   args: {
//     email: { type: GraphQLString },
//     password: { type: GraphQLString },
//   },
//   async resolve(parent: any, args: any) {
//     const { email, password } = args;
//     const user = await User.findOne({
//       where: { email, password },
//       select: ["name", "email", "role", "password"],
//     });
//     if (!user || user.password !== password) {
//       throw new Error("Invalid credentials");
//     }

//     const token = createdToken({
//       name: user.name,
//       email: user.email,
//       role: user.role,
//     });

//     return token;
//   },
// };
