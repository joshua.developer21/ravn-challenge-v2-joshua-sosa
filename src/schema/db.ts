import { DataSource } from "typeorm";
import { User } from "./Entities/User";
import { Product } from "./Entities/Product";
import { Category } from "./Entities/Category";

export const AppDataSource = new DataSource({
  type: "postgres",
  host: "localhost",
  username: "postgres",
  password: "admin",
  database: "rvendb",
  port: 5432,
  entities: [User, Product, Category],
  logging: true,
  synchronize: true,
});
