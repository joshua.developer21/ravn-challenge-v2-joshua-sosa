import { GraphQLSchema, GraphQLObjectType } from "graphql";
import { GREETING } from "./Queries/Greeting";
import { SIGN_UP, LOGIN, SIGN_OUT } from "./Mutations/User";
import { CREATE_PRODUCT } from "./Mutations/Product";

const RootQuery = new GraphQLObjectType({
  name: "RootQuery",
  fields: {
    greeting: GREETING,
  },
});

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    signUP: SIGN_UP,
    login: LOGIN,
    createProduct: CREATE_PRODUCT,
  },
});

export const schema = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation,
});
