import express from "express";
import { graphqlHTTP } from "express-graphql";
import { schema } from "./schema";
import cors from "cors";
import morgan from "morgan";
import { authenticate } from "./Middlewares/auth";

const app = express();

app.use(authenticate);

app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    rootValue: {},
    graphiql: true,
  })
);
app.use(morgan("dev"));
app.use(cors());

export default app;
